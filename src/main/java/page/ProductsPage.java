package page;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.testng.Assert;
import io.qameta.allure.Step;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import com.codeborne.selenide.Condition;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.Selenide.$$x;
import static com.codeborne.selenide.Selenide.$x;

public class ProductsPage{

    List<String> products = Arrays.asList(
            "Sauce Labs Backpack",
            "Sauce Labs Bike Light",
            "Sauce Labs Bolt T-Shirt",
            "Sauce Labs Fleece Jacket",
            "Sauce Labs Onesie",
            "Test.allTheThings() T-Shirt (Red)"
    );
    ElementsCollection prod = $$x("//div[@class ='inventory_item_name ']");
    SelenideElement logout = $x("//*[text()='Logout']");
    SelenideElement menuBtn = $x("//button[@id = 'react-burger-menu-btn']");


    @Step("Ожидание страницы с продуктами")
    public ProductsPage checkElement() {
        $x("//*[text()='Products']").should(Condition.appear);
        $x("//a[@class ='shopping_cart_link']").should(Condition.appear);
        $x("//select[@class ='product_sort_container']").should(Condition.appear);
        $x("//div[@class='footer_copy']").should(Condition.appear);
        $x("//*[text()= ' Sauce Labs. All Rights Reserved. Terms of Service | Privacy Policy']").should(Condition.appear);

        return this;
    }

    @Step("Сортировка")
    public ProductsPage checkSort() {
        products.sort(String.CASE_INSENSITIVE_ORDER);

        for (int i = 0; i < 6; i++) {
            if (!prod.get(i).getText().equals(products.get(i))) {
                Assert.fail("Ошибка в сортировке");
            }
        }
        return this;
    }

    @Step("Добавлени и удаление товаров")
    public ProductsPage checkAddAndRemoveProducts() {
        ElementsCollection btn = $$x("//div[@class ='pricebar']//button");

        for (int i = 0; i < 6; i++) {
            btn.get(i).click();
        }

        String basket = $x("//span[@class ='shopping_cart_badge']").getText();
        Assert.assertTrue(basket.contains("6"));

        for (int i = 0; i < 6; i++) {
            btn.get(i).click();
        }
        return this;
    }

    @Step("Обратная сортировка")
    public ProductsPage reverseSort() {
        $x("//select[@class='product_sort_container']").click();
        $x("//option[@value='za']").click();

        prod = $$x("//div[@class ='inventory_item_name ']");
        products.sort(String.CASE_INSENSITIVE_ORDER);

        for (int i = 0; i < 6; i++) {
            if (!prod.get(i).getText().equals(products.get(5-i))) {
                Assert.fail("Ошибка в сортировке");
            }
        }

        return this;
    }

    @Step("Нажатие Menu и ожидание её внутренний элементов")
    public ProductsPage checkMenuElements() {
        menuBtn.click();

        $x("//*[text()='All Items']").should(Condition.appear);
        $x("//*[text()='About']").should(Condition.appear);
        logout.should(Condition.appear, Duration.ofSeconds(2));
        $x("//*[text()='Reset App State']").should(Condition.appear);
        $x("//button[@id='react-burger-cross-btn']").click();

        return this;
    }

    @Step("Выход")
    public ProductsPage logout() {
        menuBtn.click();
        sleep(2);
        logout.click();

        $x("//input[@placeholder = 'Username']").should(Condition.appear);
        $x("//input[@placeholder = 'Password']").should(Condition.appear);
        $x("//input[@value = 'Login']").should(Condition.appear);

        return this;
    }

    @Step("Добавление продукта в корзину")
    public ProductsPage addProductInBasket() {
        $x("//div[@class ='pricebar']//button").click();
        $x("//span[@class='shopping_cart_badge' and text()='1']").should(Condition.appear);

        products = null;
        products = Arrays.asList(
                $x("//div[@class='inventory_item_name ']").getText(),
                $x("//div[@class='inventory_item_desc']").getText(),
                $x("//div[@class='inventory_item_price']").getText()
                );

        return this;
    }

    @Step("Проверка корзины")
    public ProductsPage checkBasket() {
        $x("//a[@class = 'shopping_cart_link']").click();
        $x("//span[text()='Your Cart']").should(Condition.appear);
        $x("//div[text()='QTY']").should(Condition.appear);
        $x("//div[text()='Description']").should(Condition.appear);

        List<String> productInBasket = Arrays.asList(
                $x("//div[@class='inventory_item_name']").getText(),
                $x("//div[@class='inventory_item_desc']").getText(),
                $x("//div[@class='inventory_item_price']").getText()
        );

        for (int i = 0; i < 3; i++) {
            if (!products.get(i).equals(productInBasket.get(i))) {
                Assert.fail("Ошибка в списке продуктов");
            }
        }
        return this;
    }

    @Step("Удаление продукта из корзины")
    public ProductsPage removeProduct() {
        $x("//button[@id='remove-sauce-labs-backpack']").click();

        return this;
    }
}
