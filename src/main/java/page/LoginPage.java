package page;
import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import static com.codeborne.selenide.Selenide.$x;

public class LoginPage{
    private SelenideElement username = $x("//input[@placeholder = 'Username']");
    private SelenideElement password = $x("//input[@placeholder = 'Password']");
    private SelenideElement btn = $x("//input[@value = 'Login']");

    public LoginPage openLoginPage(){
        Selenide.open("https://www.saucedemo.com/");

        return this;
    }

    @Step("Войти")
    public LoginPage clickLogin(){
        btn.click();

        return this;
    }

    @Step("Заполнение полей")
    public LoginPage fillForm(String usernameText, String passwordText){
        username.setValue(usernameText);
        password.setValue(passwordText);

        return this;
    }



}
