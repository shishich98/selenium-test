package common;

public class Config {
    public static final Boolean HOLD_BROWSER_OPEN = true;
    public static final Boolean CLEAR_COOKIES = true;
}
