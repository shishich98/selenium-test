package steps;
import page.LoginPage;
import page.ProductsPage;

public class Step {

    public static void login(String login, String password) {
        LoginPage loginPage = new LoginPage();
        loginPage.openLoginPage()
                .fillForm(login,password)
                .clickLogin();
    }

    public static void testFullActionsOnProductPage() {
        ProductsPage productsPage = new ProductsPage();
        productsPage.checkElement()
                .checkSort()
                .checkAddAndRemoveProducts()
                .reverseSort()
                .checkMenuElements()
                .logout();
    }

    public static void testAddProductAndRemoveProductInBasketPage() {
        ProductsPage productsPage = new ProductsPage();
        productsPage.checkElement()
                .checkSort()
                .addProductInBasket()
                .checkBasket()
                .removeProduct()
                .logout();

    }
}
