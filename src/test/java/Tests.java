import steps.Step;
import org.testng.annotations.Test;

public class Tests extends BaseTest {

    private final String passwordText = "secret_sauce";

    @Test
    public void test5321() {
        Step.login("standard_user",passwordText);
        Step.testFullActionsOnProductPage();
    }

    @Test
    public void test5322() {
        Step.login("locked_out_user",passwordText);
        Step.testFullActionsOnProductPage();
    }

    @Test
    public void test5323() {
        Step.login("problem_user",passwordText);
        Step.testFullActionsOnProductPage();

    }

    @Test
    public void test5324() {
        Step.login("performance_glitch_user",passwordText);
        Step.testFullActionsOnProductPage();
    }

    @Test
    public void test7578() {
        Step.login("standard_user",passwordText);
        Step.testAddProductAndRemoveProductInBasketPage();
    }
}
