import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {
    public void ChromeDriver() {

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--test-type");
        options.addArguments("--allow-running-insecure-content");
        options.addArguments("--ignore-certificate-errors");
        options.addArguments("--disable-ipv6");
        options.addArguments("--remote-allow-origins=*");

        WebDriverManager.chromedriver().setup();
        Configuration.browser = "chrome";
        Configuration.browserSize = "1920x1080";
        Configuration.browserCapabilities = new DesiredCapabilities().merge(options);


    }

    @BeforeMethod
    public void init(){
        ChromeDriver();
    }

    @AfterMethod
    public void tearDown(){
        Selenide.closeWebDriver();
    }
}
